# suggest-tour-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Kelompok
- 195411050 Farhan Kurnia Ragil Syahputra
- 195411020 Muhammad Afif Alfiano Hermasyah
- 195411087 Muhammad Ridho Sulistiawan
- 195411077 Nur Ikhwan Budi Santoso

### Link Project Backend
- https://gitlab.com/FarhanKurnia/REST-API-PWA-Lumen

### Link Project Live
- https://suggest-tour-app.vercel.app/
